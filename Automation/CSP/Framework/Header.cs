﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework
{
    public class Header
    {
        public static IWebElement LoanIdDropDown => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-account.hidden-xs > ul:nth-child(2) > li.dropdown > a"));
        public static IWebElement UserDropdown => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-account.hidden-xs > ul:nth-child(1) > li:nth-child(1) > a"));
        public static IWebElement LogOutBtn => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-account.hidden-xs > ul:nth-child(1) > li.open > ul > li:nth-child(6) > a"));
        public static IWebElement PageHeader => Driver.Instance.FindElement(By.TagName("h3"));
    }
}
