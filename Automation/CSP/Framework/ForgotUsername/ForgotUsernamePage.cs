﻿using OpenQA.Selenium;
using Framework;
using System.Threading;

namespace CSP.Framework.ForgotUsername
{
    public  class ForgotUsernamePage
    {
        public static string webAddress => WebDriverSettingsReader.GetBaseUrl() + "/#/user/account/forgot-username";
        public static IWebElement LoanIdFld => Driver.Instance.FindElement(By.Name("loanNumber"));
        public static IWebElement EmailFld => Driver.Instance.FindElement(By.Name("email"));
        public static IWebElement LoanErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(2) > label"));
        public static IWebElement EmailErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(4) > label"));
        public static IWebElement CancelBtn => Driver.Instance.FindElement(By.ClassName("btn-default"));
        public static IWebElement NextBtn => Driver.Instance.FindElement(By.ClassName("btn-primary"));
    }
}