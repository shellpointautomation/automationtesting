﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.MyProfile
{
    public class NotificationPreferencesPage
    {
        public static void GoTo(string username ="TestAccount1")
        {
            Activity.HomePage.GoTo(username);
            NavBar.NavBarPage.MyProfile.Click();
            NavBar.MyProfileDropdown.NotificationPreferences.Click();
            WaitForPageToLoad.Wait();
        }
        public static IWebElement BillingInformation => Driver.Instance.FindElement(By.CssSelector("body > section > div > div.container.content.ng-scope > div > div.col-sm-9.col-lg-10 > form > div:nth-child(4) > div > div:nth-child(2) > div:nth-child(4)"));
        public static IWebElement AddPhoneNumberLink => Driver.Instance.FindElement(By.CssSelector("a[data-toggle='modal']"));
        public static void ExpandCarret()
        {
            Driver.Instance.FindElement(By.ClassName("fa-angle-right")).Click();
        }
    }
}
