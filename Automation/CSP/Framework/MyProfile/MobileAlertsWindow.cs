﻿using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.MyProfile
{
    public class MobileAlertsWindow
    {
        public static IWebElement AddPhoneNumberBtn => Driver.Instance.FindElement(By.ClassName("btn-sm"));
        public static IWebElement TermsCheckBox => Driver.Instance.FindElement(By.Name("termsAccepted"));
        public static IWebElement SubmitBtn => Driver.Instance.FindElement(By.CssSelector("#phoneNumbersModal > div > div > div.modal-footer > button"));
        public static IWebElement PhoneNumberField => Driver.Instance.FindElement(By.Name("phoneNumber0"));
        public static IWebElement PhoneNumberCheckbox => Driver.Instance.FindElement(By.Name("selected0"));
    }
}
