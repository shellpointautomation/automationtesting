﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.Login
{
    public class LoginPage
    {
        public static void GoTo()
        {
            string baseUrl = WebDriverSettingsReader.GetBaseUrl();
            Driver.Instance.Navigate().GoToUrl(baseUrl);
            WaitForPageToLoad.Wait();
        }
        public static IWebElement UsernameFld=>Driver.Instance.FindElement(By.Name("username"));
        public static IWebElement NextBtn=> Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(3) > div > button"));
        public static IWebElement ForgotPasswordLink=>Driver.Instance.FindElement(By.LinkText("Forgot your password?"));
        public static IWebElement ForgotUsernameLink=>Driver.Instance.FindElement(By.LinkText("Forgot your username?"));
        public static IWebElement RegisterHereLink=>Driver.Instance.FindElement(By.LinkText("Register here"));
        public static IWebElement AnimationFade=>Driver.Instance.FindElement(By.ClassName("block-ui-anim-fade"));
        public static IWebElement BlockOverlay => Driver.Instance.FindElement(By.ClassName("block-ui-overlay"));
        public static IWebElement UsernameErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(2) > label"));
    }
}
