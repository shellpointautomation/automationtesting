﻿using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.Login
{
    public class AuthorizePCPage
    {
        public static string webAddress => WebDriverSettingsReader.GetBaseUrl()+"/#/user/account/authorize-pc";
        public static IWebElement YesToggle => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(1) > div > div > label > input"));
        public static IWebElement NoToggle => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div.form-group.has-feedback > div > div > label > input"));
        public static IWebElement NextBtn => Driver.Instance.FindElement(By.ClassName("btn-primary"));
        public static IWebElement CancelBtn => Driver.Instance.FindElement(By.ClassName("btn-default"));
    }
}
