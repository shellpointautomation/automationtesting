﻿using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.Login
{
    public class PasswordPage
    {
        public static IWebElement PasswordFld => Driver.Instance.FindElement(By.Name("password"));
        public static IWebElement SubmitBtn => Driver.Instance.FindElement(By.ClassName("btn-primary"));
        public static IWebElement CancelBtn => Driver.Instance.FindElement(By.ClassName("btn-default"));
        public static IWebElement ErrorMessage => Driver.Instance.FindElement(By.Id("toast-container"));
    }
}
