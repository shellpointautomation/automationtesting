﻿using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.NavBar
{
    public class ActivityDropdown 
    {
        public static IWebElement Summary => Driver.Instance.FindElement(By.CssSelector("a[href$='summary']"));
        public static IWebElement Detail => Driver.Instance.FindElement(By.CssSelector("a[href$='detail']"));
        public static IWebElement PaymentHistory => Driver.Instance.FindElement(By.CssSelector("a[href$='history']"));
    }
}
