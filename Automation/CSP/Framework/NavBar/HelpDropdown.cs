﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.NavBar
{
    public class HelpDropdown
    {
        public static IWebElement FAQs => Driver.Instance.FindElement(By.CssSelector("a[href $= 'faqs'"));
        public static IWebElement ContactUs => Driver.Instance.FindElement(By.CssSelector("a[href $= 'contact-us'"));
    }
}
