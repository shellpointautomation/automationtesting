﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.NavBar
{
    public class ServicesDropdown
    {
        public static IWebElement ContactPreferences => Driver.Instance.FindElement(By.CssSelector("a[href $= 'contact-preferences'"));
        public static IWebElement AvailableDocuments => Driver.Instance.FindElement(By.CssSelector("a[href $= 'available'"));
        public static IWebElement LossMitDocuments => Driver.Instance.FindElement(By.CssSelector("a[href $= 'lossmitigationdocuments'"));
        public static IWebElement HomeOwnerAssistance => Driver.Instance.FindElement(By.CssSelector("a[href $= 'homeowner-assistance-solutions'"));

    }
}
