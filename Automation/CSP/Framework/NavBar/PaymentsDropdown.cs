﻿using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.NavBar
{
    public class PaymentsDropdown
    {
        public static IWebElement SchedulePayments => Driver.Instance.FindElement(By.CssSelector("a[href$='one-time'"));
        public static IWebElement PendingPayment => Driver.Instance.FindElement(By.CssSelector("a[href$='pending'"));
        public static IWebElement RecurringPayments => Driver.Instance.FindElement(By.CssSelector("a[href$='recurring'"));
        public static IWebElement RequestPayoff => Driver.Instance.FindElement(By.CssSelector("a[href$='request-payoff'"));
    }
}
