﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.NavBar
{
    public class StatementsDropdown
    {
        public static IWebElement Monthly => Driver.Instance.FindElement(By.CssSelector("a[href $= 'monthly']"));
        public static IWebElement Yearly => Driver.Instance.FindElement(By.CssSelector("a[href $= 'yearly']"));
        public static IWebElement TaxesInsurance => Driver.Instance.FindElement(By.CssSelector("a[href $= 'taxes-insurance']"));
    }
}
