﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.NavBar
{
    public class MyProfileDropdown
    {
        public static IWebElement Preferences => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-main.ng-scope > ul > li.dropdown.open > ul > li:nth-child(1) > a"));
        public static IWebElement ChangePassword => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-main.ng-scope > ul > li.dropdown.open > ul > li:nth-child(2) > a"));
        public static IWebElement SecurityQuestions => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-main.ng-scope > ul > li.dropdown.open > ul > li:nth-child(3) > a"));
        public static IWebElement NotificationPreferences => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-main.ng-scope > ul > li.dropdown.open > ul > li:nth-child(4) > a"));
        public static IWebElement TermsConditions => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-main.ng-scope > ul > li.dropdown.open > ul > li:nth-child(5) > a"));
        public static IWebElement SignOut => Driver.Instance.FindElement(By.CssSelector("body > header > div > div > nav > div > div.collapse.navbar-collapse.navbar-top-collapse.navbar-main.ng-scope > ul > li.dropdown.open > ul > li:nth-child(6) > a"));
    }
}
