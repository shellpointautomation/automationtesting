﻿using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.NavBar
{
    public class NavBarPage
    {
        public static IWebElement Activity => Driver.Instance.FindElement(By.CssSelector("[name = 'activity'][data-toggle = 'dropdown']"));
        public static IWebElement Payments => Driver.Instance.FindElement(By.CssSelector("[name = 'payments'][data-toggle = 'dropdown']"));
        public static IWebElement Statements => Driver.Instance.FindElement(By.CssSelector("[name = 'statements'][data-toggle = 'dropdown']"));
        public static IWebElement OnlineServices => Driver.Instance.FindElement(By.CssSelector("[name = 'services'][data-toggle = 'dropdown']"));
        public static IWebElement Help => Driver.Instance.FindElement(By.CssSelector("[name = 'help'][data-toggle = 'dropdown']"));
        public static IWebElement MyProfile => Driver.Instance.FindElement(By.CssSelector("[name = 'profile']"));
    }
}
