﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.RegisterHere
{
    public class RegisterStep1Page
    {
        public static string webAddress => WebDriverSettingsReader.GetBaseUrl() + "/#/user/registration/step1";
        public static IWebElement LoanIdFld => Driver.Instance.FindElement(By.Name("loanNumber"));
        public static IWebElement FirstNameFld => Driver.Instance.FindElement(By.Name("firstName"));
        public static IWebElement LastNameFld => Driver.Instance.FindElement(By.Name("lastName"));
        public static IWebElement EmailFld => Driver.Instance.FindElement(By.Name("email"));
        public static IWebElement ConfirmEmailFld => Driver.Instance.FindElement(By.Name("confirmEmail"));
        public static IWebElement SSNFld => Driver.Instance.FindElement(By.Name("id"));
        public static IWebElement ZipCodeFld => Driver.Instance.FindElement(By.Name("zipCode"));
        public static IWebElement BusinessName => Driver.Instance.FindElement(By.Name("businessName"));

        public static IWebElement LoanNumberErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(3) > label"));
        public static IWebElement FirstNameErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(5) > label"));
        public static IWebElement LastNameErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(7) > label"));
        public static IWebElement EmailErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(9) > label"));
        public static IWebElement ConfirmEmailErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(11) > label"));
        public static IWebElement SSNErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(7) > label"));
        public static IWebElement ZipCodeMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(7) > label"));
        public static IWebElement CancelBtn => Driver.Instance.FindElement(By.ClassName("btn-default"));
        public static IWebElement NextBtn => Driver.Instance.FindElement(By.ClassName("btn-primary"));
        public static IWebElement Toast => Driver.Instance.FindElement(By.Id("toast-container"));
    }
}
