﻿using Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace CSP.Framework
{
    public class WaitForPageToLoad
    {
        public static IWebElement AnimationFade => Driver.Instance.FindElement(By.ClassName("block-ui-anim-fade"));
        public static IWebElement BlockOverlay => Driver.Instance.FindElement(By.ClassName("block-ui-overlay"));
        public static IWebElement Message => Driver.Instance.FindElement(By.ClassName("block-ui-message"));
        public static void Wait()
        {
            try
            {
                Thread.Sleep(500);
                SpinWait.SpinUntil(() => (AnimationFade.GetAttribute("aria-busy") == "false"), 15000);
                DriverExtensions.WaitForElementDissapears(BlockOverlay);
                DriverExtensions.WaitForElementDissapears(Message);
                Thread.Sleep(100);
            }
            catch (Exception e)
            {
                Console.WriteLine("Wait for page exception:"+e);
            }  
        }
    }
}
