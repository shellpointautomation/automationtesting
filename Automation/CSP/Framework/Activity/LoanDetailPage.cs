﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.Activity
{
    public class LoanDetailPage
    {
        public static IWebElement Borrower => Driver.Instance.FindElement(By.CssSelector("body > section > div > div.container.content.ng-scope > div > div.col-sm-9.col-lg-10 > div > div.container-fluid > div > div:nth-child(1)"));
    }
}
