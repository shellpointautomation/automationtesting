﻿using CSP.Framework.Login;
using Framework;
using OpenQA.Selenium;

namespace CSP.Framework.Activity
{
    public class HomePage
    {
        public static IWebElement MakePaymentTile => Driver.Instance.FindElement(By.CssSelector("a[href$='one-time']"));
        public static IWebElement ReturnToHomeLogo => Driver.Instance.FindElement(By.ClassName("navbar-brand"));
        public static void GoTo(string username = "TestAccount1")
        {
            LoginPage.GoTo();
            LoginPage.UsernameFld.SendKeys(username);
            LoginPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            AuthorizePCPage.NoToggle.Click();
            AuthorizePCPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            PasswordPage.PasswordFld.SendKeys(username);
            PasswordPage.SubmitBtn.Click();
            WaitForPageToLoad.Wait();
            WaitForPageToLoad.Wait();
        }

    }
}
