﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.Activity
{
    public class PaymentHistory
    {
        public static IWebElement TransactionHeader => Driver.Instance.FindElement(By.CssSelector("body > section > div > div.container.content.ng-scope > div > div.col-sm-9.col-lg-10 > div > div:nth-child(1) > table > thead > tr > th:nth-child(1)"));
    }
}
