﻿using OpenQA.Selenium;
using Framework;
using CSP.Framework.Login;

namespace CSP.Framework.ForgotPassword
{
    public class ForgotPasswordStep1Page
    {
        public void GoTo()
        {
            LoginPage.ForgotPasswordLink.Click();
            WaitForPageToLoad.Wait();
        }
        public static string webAddress = WebDriverSettingsReader.GetBaseUrl()+ "/#/user/forgot-password/step1";
        public static IWebElement UsernameFld => Driver.Instance.FindElement(By.Name("username"));
        public static IWebElement CancelBtn => Driver.Instance.FindElement(By.ClassName("btn-default"));
        public static IWebElement NextBtn => Driver.Instance.FindElement(By.ClassName("btn-primary"));
        public static IWebElement SignInLink => Driver.Instance.FindElement(By.LinkText("Sign in"));
        public static IWebElement ProgressBar => Driver.Instance.FindElement(By.ClassName("progress"));
    }
}
