﻿using OpenQA.Selenium;
using Framework;

namespace CSP.Framework.ForgotPassword
{
    class ForgotPasswordStep2Page
    {
        public static IWebElement AnswerFld => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div.form-group.has-feedback > div > div > input"));
        public static IWebElement CancelBtn => Driver.Instance.FindElement(By.ClassName("btn-default"));
        public static IWebElement SubmitBtn => Driver.Instance.FindElement(By.ClassName("btn-primary"));
        public static IWebElement ErrorMessage => Driver.Instance.FindElement(By.CssSelector("body > section > div > div > div > form > div:nth-child(3) > label"));
        public static IWebElement ProgressBar => Driver.Instance.FindElement(By.ClassName("progress"));
    }
}
