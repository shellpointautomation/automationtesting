﻿using CSP.Framework;
using CSP.Framework.MyProfile;
using NUnit.Framework;
using Tests;
namespace CSP.Tests.MyProfile
{
    [TestFixture]
    public class NotificationPreferencesTests : BaseTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            NotificationPreferencesPage.GoTo();
        }
        [Test]
        public void ExpandCarret()
        {
            NotificationPreferencesPage.ExpandCarret();
            Assert.IsTrue(NotificationPreferencesPage.BillingInformation.Displayed);
        }
        [Test]
        public void OpenNewPhoneNumberLink()
        {
            NotificationPreferencesPage.AddPhoneNumberLink.Click();
            Assert.IsTrue(MobileAlertsWindow.SubmitBtn.Displayed);
        }     

    }
}
