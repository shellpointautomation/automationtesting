﻿using CSP.Framework.MyProfile;
using NUnit.Framework;
using Tests;

namespace CSP.Tests.MyProfile
{
    [TestFixture]
    public class MobileAlertsWindowTests : BaseTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            NotificationPreferencesPage.GoTo();
            NotificationPreferencesPage.AddPhoneNumberLink.Click();
        }
        [Test]
        public void AddPhoneNumber()
        {
            MobileAlertsWindow.AddPhoneNumberBtn.Click();
            MobileAlertsWindow.PhoneNumberField.SendKeys("7045621008");
            MobileAlertsWindow.PhoneNumberCheckbox.Click();
            MobileAlertsWindow.TermsCheckBox.Click();
            MobileAlertsWindow.SubmitBtn.Click();
        }
    }
}
