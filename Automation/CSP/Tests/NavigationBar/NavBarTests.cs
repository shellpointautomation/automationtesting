﻿using NUnit.Framework;
using Tests;
using Framework;
using CSP.Framework.Activity;
using CSP.Framework;
using CSP.Framework.NavBar;

namespace CSP.Tests.NavigationBar
{
    [TestFixture]
    public class NavBarTests : BaseTest
    {
        /// <summary>
        /// Before all tests are run log in using TestAccount1
        /// </summary>
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            HomePage.GoTo("TestAccount1");   
        }
        /// <summary>
        /// before each test go back to the catch screen and wait for the page to load
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            WaitForPageToLoad.Wait();
            HomePage.ReturnToHomeLogo.Click();
            WaitForPageToLoad.Wait();
        }
        /// <summary>
        /// Go to the activity summary page using the NavBar
        /// and make sure you are on the correct page
        /// </summary>
        [Test]
        public void ActivitySummary()
        {
            string homePageUrl = Driver.Instance.Url.ToString();
            ActivityDropdown.Summary.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("SUMMARY", Header.PageHeader.Text);
        }
        /// <summary>
        /// Go to the activity details page using the NavBar
        /// and make sure you are on the correct page
        /// </summary>
        [Test]
        public void ActivityDetail()
        {
            NavBarPage.Activity.Click();
            ActivityDropdown.Detail.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("LOAN DETAIL", Header.PageHeader.Text);
        }
        /// <summary>
        /// Go to the activity payment page using the NavBar
        /// and make sure you are on the correct page
        /// </summary>
        [Test]
        public void ActivityPaymentHistory()
        {
            NavBarPage.Activity.Click();
            ActivityDropdown.PaymentHistory.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("PAYMENT HISTORY", Header.PageHeader.Text);
        }
        [Test]
        public void PaymentsSchedulePayment()
        {
            NavBarPage.Payments.Click();
            PaymentsDropdown.SchedulePayments.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("ONE TIME PAYMENT", Header.PageHeader.Text);
        }
        [Test]
        public void PaymentsPendingPayment()
        {
            NavBarPage.Payments.Click();
            PaymentsDropdown.PendingPayment.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("PENDING PAYMENT", Header.PageHeader.Text);
        }
        [Test]
        public void PaymentsRecurringPayment()
        {
            NavBarPage.Payments.Click();
            PaymentsDropdown.RecurringPayments.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("RECURRING PAYMENTS", Header.PageHeader.Text);
        }
        [Test]
        public void PaymentsRequestPayoff()
        {
            NavBarPage.Payments.Click();
            PaymentsDropdown.RequestPayoff.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("REQUEST PAYOFF", Header.PageHeader.Text);
        }
        [Test]
        public void StatementsMonthly()
        {
            NavBarPage.Statements.Click();
            StatementsDropdown.Monthly.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("MONTHLY STATEMENTS", Header.PageHeader.Text);
        }
        [Test]
        public void StatementsYearly()
        {
            NavBarPage.Statements.Click();
            StatementsDropdown.Yearly.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("YEARLY STATEMENTS", Header.PageHeader.Text);
        }
        [Test]
        public void StatementsTaxesInsurance()
        {
            NavBarPage.Statements.Click();
            StatementsDropdown.TaxesInsurance.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("TAXES & INSURANCE", Header.PageHeader.Text);
        }
        [Test]
        public void ServicesContactPreferences()
        {
            NavBarPage.OnlineServices.Click();
            ServicesDropdown.ContactPreferences.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("CONTACT PREFERENCES", Header.PageHeader.Text);
        }
        [Test]
        public void ServicesAvailableDocuments()
        {
            NavBarPage.OnlineServices.Click();
            ServicesDropdown.AvailableDocuments.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("AVAILABLE DOCUMENTS", Header.PageHeader.Text);
        }
        [Test]
        public void ServicesLossMitDocuments()
        {
            NavBarPage.OnlineServices.Click();
            ServicesDropdown.LossMitDocuments.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("LOSS MITIGATION DOCUMENTS", Header.PageHeader.Text);
        }
        [Test]
        public void ServicesHomeOwnerAssistance()
        {
            NavBarPage.OnlineServices.Click();
            ServicesDropdown.HomeOwnerAssistance.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("HOMEOWNER ASSISTANCE SOLUTIONS", Header.PageHeader.Text);
        }
        [Test]
        public void HelpFAQs()
        {
            NavBarPage.Help.Click();
            HelpDropdown.FAQs.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("FAQS", Header.PageHeader.Text);
        }
        [Test]
        public void HelpContactUs()
        {
            NavBarPage.Help.Click();
            HelpDropdown.ContactUs.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("CONTACT US", Header.PageHeader.Text);
        }
        [Test]
        public void MyProfilePreferences()
        {
            NavBarPage.MyProfile.Click();
            MyProfileDropdown.Preferences.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("PREFERENCES", Header.PageHeader.Text);
        }
        [Test]
        public void MyProfileChangePassword()
        {
            NavBarPage.MyProfile.Click();
            System.Threading.Thread.Sleep(100);
            MyProfileDropdown.ChangePassword.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("CHANGE PASSWORD", Header.PageHeader.Text);
        }
        [Test]
        public void MyProfileSecurityQuestions()
        {
            NavBarPage.MyProfile.Click();
            MyProfileDropdown.SecurityQuestions.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("SECURITY QUESTIONS", Header.PageHeader.Text);
        }
        [Test]
        public void MyProfileNotificationPreferences()
        {
            NavBarPage.MyProfile.Click();
            MyProfileDropdown.NotificationPreferences.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("NOTIFICATION PREFERENCES", Header.PageHeader.Text);
        }
        [Test]
        public void MyProfileTermsConditions()
        {
            NavBarPage.MyProfile.Click();
            MyProfileDropdown.TermsConditions.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("TERMS AND CONDITIONS", Header.PageHeader.Text);
        }

    }
}
