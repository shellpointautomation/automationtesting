﻿using CSP.Framework;
using CSP.Framework.Activity;
using CSP.Framework.NavBar;
using NUnit.Framework;
using Tests;

namespace CSP.Tests.NavigationBar
{
    [TestFixture]
    public class LogOutTests : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            HomePage.GoTo("TestAccount1");
        }
        [Test]
        public void NavBarLogOut()
        {
            WaitForPageToLoad.Wait();
            NavBarPage.MyProfile.Click();
            MyProfileDropdown.SignOut.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("SIGN IN: USERNAME", Header.PageHeader.Text);
        }
        [Test]
        public void HeaderLogOut()
        {
            WaitForPageToLoad.Wait();
            Header.UserDropdown.Click();
            Header.LogOutBtn.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual("SIGN IN: USERNAME", Header.PageHeader.Text);
        }
    }
}
