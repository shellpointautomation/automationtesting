﻿using NUnit.Framework;
using Tests;
using Framework;
using CSP.Framework.Login;
using CSP.Framework;
using CSP.Framework.NavBar;

namespace CSP.Tests.Login
{
    [TestFixture]
    public class LoginTests : BaseTest
    {
        [OneTimeSetUp]
        public override void LaunchBrowser()
        {
        }
        /// <summary>
        /// happenes before each test, Launches browser and goes to the login page
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            Driver.Initialize();
            LoginPage.GoTo();
        }

        /// <summary>
        /// Logging in with valid credentials, make sure you are taken
        /// to the homepage successfully 
        /// </summary>
        [Test]
        [TestCase("TestAccount1", "TestAccount1")]
        [TestCase("TestAccount2", "TestAccount2")]
        public void ValidCredentials(string username, string password)
        {
            LoginPage.UsernameFld.SendKeys(username);
            LoginPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            AuthorizePCPage.YesToggle.Click();
            AuthorizePCPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            PasswordPage.PasswordFld.SendKeys(password);
            PasswordPage.SubmitBtn.Click();
            WaitForPageToLoad.Wait();
            Assert.NotNull(NavBarPage.Activity.Displayed);
        }
        /// <summary>
        /// Attempt to log in with invalid credentials, Should get an error
        /// message when entering the password
        /// </summary>
        [Test]
        [TestCase("TestAccount1", "WrongPassword1")]
        [TestCase("TestAccount1", "WrongPassword2")]
        public void IncorrectPassword(string username, string password)
        {
            LoginPage.UsernameFld.SendKeys(username);
            LoginPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            AuthorizePCPage.YesToggle.Click();
            AuthorizePCPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            PasswordPage.PasswordFld.SendKeys(password);
            PasswordPage.SubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad(PasswordPage.ErrorMessage);
            Assert.NotNull(PasswordPage.ErrorMessage.Displayed);
        }
        /// <summary>
        /// after each test close the browser, If test fails take a screenshot
        /// </summary>
        [TearDown]
        override public void TearDown()
        {
            base.TearDown();
            Driver.Instance.Close();
        }
        [OneTimeTearDown]
        public override void CloseBrowser()
        {
        }
    }
}
