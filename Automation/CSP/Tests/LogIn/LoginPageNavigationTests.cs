﻿using Framework;
using NUnit.Framework;
using Tests;
using CSP.Framework.Login;
using CSP.Framework;
using CSP.Framework.ForgotUsername;
using CSP.Framework.ForgotPassword;
using CSP.Framework.RegisterHere;

namespace CSP.Tests.Login
{
    [TestFixture]
    public class LoginPageNavigationTests : BaseTest
    {
        /// <summary>
        /// Go to the Login Page before each test in the class
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            LoginPage.GoTo();
        }
        /// <summary>
        /// checking that an error message is thrown when trying to click 
        /// next with nothing in the username field
        /// </summary>
        [Test]
        public void NoUsernameNext()
        {
            LoginPage.NextBtn.Click();
            Assert.NotNull(LoginPage.UsernameErrorMessage.Displayed);
        }
        /// <summary>
        /// Clicking next will take you to the next step of the login process
        /// </summary>
        [Test]
        public void UserNameFld()
        {
            LoginPage.UsernameFld.SendKeys("TestAccount1");
            LoginPage.NextBtn.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual(AuthorizePCPage.webAddress, Driver.Instance.Url.ToString());
        }
        /// <summary>
        /// Checking the forgot username link is working as expected
        /// </summary>
        [Test]
        public void ForgotUserNameLink()
        {
            LoginPage.ForgotUsernameLink.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual(ForgotUsernamePage.webAddress, Driver.Instance.Url.ToString());
        }
        /// <summary>
        /// Checking the forgot password link is working as expected
        /// </summary>
        [Test]
        public void ForgotPasswordLink()
        {
            LoginPage.ForgotPasswordLink.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual(ForgotPasswordStep1Page.webAddress, Driver.Instance.Url.ToString());
        }
        /// <summary>
        /// Checking the register here link is working as expected
        /// </summary>
        [Test]
        public void RegisterHereLink()
        {
            LoginPage.RegisterHereLink.Click();
            WaitForPageToLoad.Wait();
            Assert.AreEqual(RegisterStep1Page.webAddress, Driver.Instance.Url.ToString());
        }
    }
}
