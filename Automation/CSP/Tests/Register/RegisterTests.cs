﻿using CSP.Framework;
using CSP.Framework.Login;
using CSP.Framework.RegisterHere;
using NUnit.Framework;
using Tests;

namespace CSP.Tests.Register
{
    [TestFixture]
    class RegisterIndividualTests : BaseTest
    {
        /// <summary>
        /// before each test go to the register here page
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            LoginPage.GoTo();
            WaitForPageToLoad.Wait();
            LoginPage.RegisterHereLink.Click();
            WaitForPageToLoad.Wait();
        }
        /// <summary>
        /// Click the cancel button and make sure you are taken back to
        /// the login page
        /// </summary>
        [Test]
        public void CancelNavigatesBack()
        {
            RegisterStep1Page.CancelBtn.Click();
            WaitForPageToLoad.Wait();
            Assert.NotNull(LoginPage.NextBtn.Displayed);
        }
        /// <summary>
        /// Make sure all the error messages are displayed when no information is
        /// entered into a field
        /// </summary>
        [Test]
        public void NothingFilledIn()
        {
            RegisterStep1Page.NextBtn.Click();
            Assert.NotNull(RegisterStep1Page.LoanNumberErrorMessage.Displayed);
            Assert.NotNull(RegisterStep1Page.FirstNameErrorMessage.Displayed);
            Assert.NotNull(RegisterStep1Page.LastNameErrorMessage.Displayed);
            Assert.NotNull(RegisterStep1Page.EmailErrorMessage.Displayed);
            Assert.NotNull(RegisterStep1Page.ConfirmEmailErrorMessage.Displayed);
            Assert.NotNull(RegisterStep1Page.SSNErrorMessage.Displayed);
            Assert.NotNull(RegisterStep1Page.ZipCodeMessage.Displayed);
        }
        /// <summary>
        /// Make sure toast messages comes up at the top of the page when 
        /// incorrect information is entered
        /// </summary>
        [Test]
        public void InvalidRegistrationInformation()
        {
            RegisterStep1Page.LoanIdFld.SendKeys("11111");
            RegisterStep1Page.FirstNameFld.SendKeys("Test");
            RegisterStep1Page.LastNameFld.SendKeys("Account");
            RegisterStep1Page.EmailFld.SendKeys("test@test.com");
            RegisterStep1Page.ConfirmEmailFld.SendKeys("test@test.com");
            RegisterStep1Page.SSNFld.SendKeys("245778899");
            RegisterStep1Page.ZipCodeFld.SendKeys("28036");
            RegisterStep1Page.NextBtn.Click();
            Assert.NotNull(RegisterStep1Page.Toast.Displayed);
        }
    }
}
