﻿using NUnit.Framework;
using PreboardingWebsite.Framework.ConfirmPayment;
using Tests;


namespace Preboarding.Tests
{
    [TestFixture]
    public class ConfirmPaymentTests : BaseTest
    {
        /// <summary>
        /// SetUp user is taken to the confirm payment page
        /// </summary>
        [OneTimeSetUp]
        public static void ClassOneTimeSetUp()
        {
            ConfirmPaymentPage.GoTo("021000089", "123456789");
        }
        /// <summary>
        /// test to check the payment ammount field has the correct value
        /// </summary>
        [Test]
        public void PaymentAmount()
        {
            Assert.AreEqual("$978.10", ConfirmPaymentPage.PaymentAmount.Text);
        }
        /// <summary>
        /// test to check the draft date field has the correct value
        /// </summary>
        [Test]
        public void DraftDate()
        {
            Assert.AreEqual("6/8/18", ConfirmPaymentPage.DraftDate.Text);
        }
        /// <summary>
        /// test to check the bank account field has the correct value
        /// </summary>
        [Test]
        public void BankAccountType()
        {
            Assert.AreEqual("Checking", ConfirmPaymentPage.BankAccountType.Text);
        }
        /// <summary>
        /// test to check the bank name field has the correct value
        /// </summary>
        [Test]
        public void BankName()
        {
            Assert.AreEqual("CITIBANK NA", ConfirmPaymentPage.Bank.Text);
        }
        /// <summary>
        /// test to check the routing number field has the correct value
        /// </summary>
        [Test]
        public void RoutingNumber()
        {
            Assert.AreEqual("021000089", ConfirmPaymentPage.RoutingNumber.Text);
        }
        /// <summary>
        /// test to check the account number field has the correct value
        /// </summary>
        [Test]
        public void AccountNumber()
        {
            Assert.AreEqual("************6789", ConfirmPaymentPage.AccountNumber.Text);
        }
    }
}
