﻿using NUnit.Framework;
using PreboardingWebsite.Framework.ConfirmPayment;
using PreboardingWebsite.Framework.MakePayment;
using Tests;

namespace Preboarding.Tests
{
    [TestFixture]
    public class MakePaymentTests : BaseTest
    {
        [SetUp()]
        public void SetUp()
        {
            MakePaymentPage.GoTo();
        }
        /// <summary>
        /// Testing that you are able to go the next page when entering valid bank information
        /// </summary>
        /// <param name="routingNumber"></param>
        [Test]
        [TestCase("021000089" )]
        [TestCase("121002042" )]
        [TestCase("082000549" )]
        [TestCase("122105045" )]
        public void ValidRoutingNumber(string routingNumber)
        {
            string accountNumber = "1234567890";

            MakePaymentPage.MakePayment(routingNumber, accountNumber);
            Assert.IsNotNull(ConfirmPaymentPage.CancelBtn.Displayed);           
        }
        /// <summary>
        /// Validating you are unable to get to the next page when using invalid routing numbers
        /// </summary>
        /// <param name="routingNumber"></param>
        [Test]
        [TestCase("1021000089")]
        [TestCase("021002042")]
        [TestCase("032000549")]
        [TestCase("123105045")]
        public void InvalidRoutingNumber(string routingNumber)
        {
            string accountNumber = "1234567890";

            MakePaymentPage.RoutingNumber.SendKeys(routingNumber);
            MakePaymentPage.AccountNumber.SendKeys(accountNumber);
            System.Threading.Thread.Sleep(3000);
            MakePaymentPage.SubmitBtn.Click();            
            Assert.AreEqual(MakePaymentPage.bankNameErrorMessage, MakePaymentPage.BankNameError.Text);
        }
        /// <summary>
        /// Enter a valid routing number. Then clear the routing number and enter and invalid
        /// Make sure you aren't able to the get to the next page
        /// </summary>
        [Test]
        public void ValidRoutingChangedToInvalid()
        {
            string accountNumber = "1234567890";
            string validRouting = "122105045";
            string invalidRouting = "000";
            //enter valid information
            MakePaymentPage.RoutingNumber.SendKeys(validRouting);
            MakePaymentPage.AccountNumber.SendKeys(accountNumber);
            MakePaymentPage.WaitForBankName();
            //Change valid rounting to invalid
            MakePaymentPage.RoutingNumber.Clear();
            MakePaymentPage.RoutingNumber.SendKeys(invalidRouting);
            //attempt to move to go to next page
            MakePaymentPage.SubmitBtn.Click();
            Assert.AreEqual(MakePaymentPage.bankNameErrorMessage, MakePaymentPage.BankNameError.Text);
        }
        /// <summary>
        /// Enter valid bank information. Click submit and go to the next page.
        /// Click the cancel button to go back. Then clear the routing number and enter and invalid
        /// Make sure you aren't able to the get to the next page
        /// </summary>
        [Test]
        public void ValidRoutingSubmitBackChangedToInvalid()
        {
            string accountNumber = "1234567890";
            string validRouting = "122105045";
            string invalidRouting = "000";
            //enter valid information
            MakePaymentPage.RoutingNumber.SendKeys(validRouting);
            MakePaymentPage.AccountNumber.SendKeys(accountNumber);
            MakePaymentPage.WaitForBankName();
            MakePaymentPage.SubmitBtn.Click();
            //Navigate back to payment page
            ConfirmPaymentPage.CancelBtn.Click();
            //Change valid rounting to invalid
            MakePaymentPage.RoutingNumber.Clear();
            MakePaymentPage.RoutingNumber.SendKeys(invalidRouting);
            //attempt to move to go to next page
            MakePaymentPage.SubmitBtn.Click();
            Assert.AreEqual(MakePaymentPage.bankNameErrorMessage, MakePaymentPage.BankNameError.Text);
        }
        /// <summary>
        /// attempt to put invalid charachters into the account number field and go to the next page
        /// </summary>
        /// <param name="accountNumber"></param>
        [Test]
        [TestCase("ABCDEF")]
        [TestCase("!@#$%^&*()")]
        [TestCase("")]
        public void InvalidAccountNumber(string accountNumber)
        {
            string routingNumber = "122105045";

            MakePaymentPage.RoutingNumber.SendKeys(routingNumber);
            MakePaymentPage.AccountNumber.SendKeys(accountNumber);
            MakePaymentPage.WaitForBankName();
            MakePaymentPage.SubmitBtn.Click();
            Assert.AreEqual(MakePaymentPage.accountNumberErrorMessage,MakePaymentPage.AccountNumberError.Text);
        }
    }
}
