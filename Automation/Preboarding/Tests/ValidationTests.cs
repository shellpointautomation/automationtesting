﻿using NUnit.Framework;
using PreboardingWebsite.Framework.Registration;
using PreboardingWebsite.Framework.Validation;
using System.Collections.Generic;
using Tests;


namespace Preboarding.Tests
{
    [TestFixture]
    public class ValidationTests : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            RegistrationPage.GoTo();
        }
        /// <summary>
        /// Check the general verbiage is as expected for all loans
        /// </summary>
        [Test]
        [TestCaseSource("GeneralUsers")]
        public void GeneralValidationVerbiage(string loanID, string zipCode, string ssn, string borrowerName, string servicerName, string priorServicerName, string transferDate, string activationDate, string webAddress)
        {
            ValidationPage.GoTo(loanID, zipCode, ssn);
            Assert.AreEqual("Dear " + borrowerName + ",", ValidationPage.BorrowerName.Text);
            Assert.AreEqual(ValidationPage.ExpectedP1(servicerName), ValidationPage.Paragraph1.Text);
            Assert.AreEqual(ValidationPage.ExpectedP2(priorServicerName, servicerName), ValidationPage.Paragraph2.Text);
            Assert.AreEqual(ValidationPage.ExpectedP3(transferDate, priorServicerName, servicerName, activationDate, webAddress), ValidationPage.Paragraph3.Text);
        }
        /// <summary>
        /// Test to check the verbiage for loans that ACH didnt transfer
        /// </summary>
        [Test]
        public void AutoDraftDidntTransferVerbiage(string loanID, string zipCode, string ssn)
        {
            string servicerName = "New Penn Mortgage";

            ValidationPage.GoTo(loanID, zipCode, ssn);
            Assert.AreEqual(AutoDraftDidntTransferPage.ExpectedP1(servicerName), AutoDraftDidntTransferPage.Paragraph1.Text, "Paragraph1 Is Incorrect");
            Assert.AreEqual(AutoDraftDidntTransferPage.ExpectedP2, AutoDraftDidntTransferPage.Paragraph2.Text, "Paragraph2 Is Incorrect");
        }
        /// <summary>
        /// Check the verbiage is as expected for loans that ACH transfered
        /// </summary>
        [Test]
        [TestCaseSource("ACHTransferUsers")]
        public void AutoDraftTransferVerbiage(string loanID, string zipCode, string ssn, string draftDay)
        {
            string servicerName = "New Penn Servicing";
            string dateReceived = "May 7, 2018";

            ValidationPage.GoTo(loanID, zipCode, ssn);
            Assert.AreEqual(AutoDraftTransferPage.ExpectedP1(dateReceived, servicerName), AutoDraftTransferPage.Paragraph1.Text, "Paragraph1 is incorrect");
            Assert.AreEqual(AutoDraftTransferPage.ExpectedP2(draftDay), AutoDraftTransferPage.Paragraph2.Text, "Paragraph2 is incorrect");
            Assert.AreEqual(AutoDraftTransferPage.ExpectedP3(servicerName), AutoDraftTransferPage.Paragraph3.Text, "Paragraph3 is incorrect");
        }
        /// <summary>
        /// Check the verbiage is as expected for loans that didnt have ACH
        /// </summary>
        [Test]
        [TestCaseSource("NoACHUsers")]
        public void NoAutoDraftVerbiage(string loanID, string zipCode, string ssn, string dateRecieved)
        {
            ValidationPage.GoTo(loanID, zipCode, ssn);
            Assert.AreEqual(NoAutoDraftPage.ExpectedP1(dateRecieved), NoAutoDraftPage.Paragraph1.Text);
            Assert.AreEqual(NoAutoDraftPage.ExpectedP2(), NoAutoDraftPage.Paragraph2.Text);
        }
        /// <summary>
        /// Check the verbiage is as expected for loans that have already made a payment
        /// </summary>
        [Test]
        [TestCaseSource("PaymentMadeUsers")]
        public void PaymentMadeVerbiage(string loanID, string zipCode, string ssn)
        {
            PaymentMadePage.GoTo(loanID, zipCode, ssn);
            Assert.AreEqual(PaymentMadePage.Paragraph1Expected, PaymentMadePage.Paragraph1.Text);
        }
        public static IEnumerable<object[]> PaymentMadeUsers()
        {
            yield return new object[] { "0578318856", "48033", "4933" };
            yield return new object[] { "0578318857", "43613", "5411" };
            yield return new object[] { "0578350886", "20012", "3876" };
        }
        public static IEnumerable<object[]> NoACHUsers()
        {
            yield return new object[] { "0578319175", "41018", "7770", "May 7, 2018" };
            yield return new object[] { "0578356132", "20109", "3250", "May 7, 2018" };
            yield return new object[] { "0578318954", "55092", "4196", "May 7, 2018" };
        }
        public static IEnumerable<object[]> ACHTransferUsers()
        {
            yield return new object[] { "0578334959", "07733", "3764", "1st" };
            yield return new object[] { "0578335232", "78148", "6311", "2nd" };
            yield return new object[] { "0578352660", "27703", "3237", "10th" };
        }
        public static IEnumerable<object[]> GeneralUsers()
        {
            yield return new object[] { "0578319175", "41018", "7770", "DEBORAH QURAGA", "New Penn Servicing", "Capital One", "June 1, 2018", "June 8, 2018", "https://myloan.newpennservicing.com" };
            yield return new object[] { "0578356132", "20109", "3250", "RAPHAEL BILOMBA", "New Penn Servicing", "Capital One", "June 1, 2018", "June 8, 2018", "https://myloan.newpennservicing.com" };
            yield return new object[] { "0578318858", "80106", "9023", "DEBORAH REYNOLDS", "New Penn Servicing", "Capital One", "June 1, 2018", "June 8, 2018", "https://myloan.newpennservicing.com" };
        }
    }
}
