﻿using NUnit.Framework;
using Tests;
using Framework;
using PreboardingWebsite.Framework.Registration;
using PreboardingWebsite.Framework.Validation;

namespace Preboarding.Tests
{
    [TestFixture]
    public class RegistrationTests : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            RegistrationPage.GoTo();
        }
        /// <summary>
        /// Attempt to log in with valid credentials except the loan number
        /// </summary>
        [Test]
        [TestCase("1")]
        [TestCase("05783188731")]
        [TestCase("000578318873")]
        public void InvalidLoanNumber(string loanNumber)
        {
            ValidationPage.GoTo(loanNumber, "28660", "4801");
            Assert.IsNotNull(RegistrationPage.ErrorMessage.Displayed);
        }
        /// <summary>
        /// Attempt to log in with valid credentials except the zip code
        /// </summary>
        [Test]
        [TestCase( "28661")]
        [TestCase( "28600")]
        [TestCase( "00000")]
        public void InvalidZip(string zipCode )
        {
            ValidationPage.GoTo("0578319064", zipCode, "4801");
            Assert.IsNotNull(RegistrationPage.ErrorMessage.Displayed);
        }
        /// <summary>
        /// Attempt to log in with valid credentials except the SSN
        /// </summary>
        [Test]
        [TestCase("1111")]
        [TestCase("0362")]
        [TestCase("062")]
        public void InvalidSSN(string last4SSN)
        {
            ValidationPage.GoTo("0578319064", "28660", last4SSN);
            Assert.IsNotNull(RegistrationPage.ErrorMessage.Displayed);
        }
        /// <summary>
        /// Attempt to log in with valid credentials using the prior servicer laonID
        /// </summary>
        [Test]
        [TestCase("7367428", "41018", "7770")]
        [TestCase("537054660", "20109", "3250")]
        [TestCase("3754256", "80106", "9023" )]                   
        public void ValidUsingPriorLoanId(string loanNumber, string zipCode, string last4SSN)
        {
            ValidationPage.GoTo(loanNumber, zipCode, last4SSN);
            Assert.IsNotNull(ValidationPage.Paragraph1.Displayed);
        }
        /// <summary>
        /// Attempt to log in with valid credentials using the coborrowers ssn
        /// </summary>
        [Test]
        [TestCase("0578319452", "11706","0832")]
        [TestCase("0578319559", "19971", "0972")]
        [TestCase("0578319514", "20147", "3364")]
        public void ValidUsingCoBorrowerSSN(string loanNumber, string zipCode, string last4SSN)
        {
            ValidationPage.GoTo(loanNumber, zipCode, last4SSN);
            Assert.IsNotNull(ValidationPage.Paragraph1.Displayed);
        }
        /// <summary>
        /// Attempt to log in with valid credentials using CoBorrower SSN and prior servicer loanID
        /// </summary>
        [Test]
        [TestCase("553030305", "11706", "0832")]
        [TestCase("533033411", "19971", "0972")]
        [TestCase("533033957", "21702", "5297")]
        public void PriorLoanIdCoBorrowerSSN(string loanNumber, string zipCode, string last4SSN)
        {
            ValidationPage.GoTo(loanNumber, zipCode, last4SSN);
            DriverExtensions.WaitForElementToLoad(ValidationPage.SchedulePaymentBtn);
            Assert.IsNotNull(ValidationPage.Paragraph1.Displayed);
        }
        /// <summary>
        /// Click the submit button without entering any information
        /// </summary>
        [Test]
        public void SubmitBtnNoInputs()
        {
            RegistrationPage.SubmitBtn.Click();
            Assert.AreNotEqual("https://mynewloan.test.newpennservicing.com/validation-success", Driver.Instance.Url);       
        }
    }
}
