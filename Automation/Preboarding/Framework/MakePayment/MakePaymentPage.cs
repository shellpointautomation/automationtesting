﻿using OpenQA.Selenium;
using Framework;
using PreboardingWebsite.Framework.Validation;

namespace PreboardingWebsite.Framework.MakePayment
{
    public class MakePaymentPage
    {
        public static void GoTo()
        {
            ValidationPage.GoTo();
            ValidationPage.SchedulePaymentBtn.Click();
            DriverExtensions.WaitForElementToLoad(BankAccountDropDown);
        }

        public static string bankNameErrorMessage = "Routing Number Invalid";
        public static string accountNumberErrorMessage = "Account Number Required";
        public static IWebElement BankAccountDropDown=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > form > div > div > div:nth-child(1) > div > select"));
        public static IWebElement RoutingNumber=>Driver.Instance.FindElement(By.Id("routingNumber"));
        public static IWebElement AccountNumber=>Driver.Instance.FindElement(By.Id("accountNumber"));
        public static IWebElement AccountNumberError => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > form > div > div > div:nth-child(4) > div > div"));
        public static IWebElement SubmitBtn=> Driver.Instance.FindElement(By.Id("submit"));
        public static IWebElement BankName=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > form > div > div > div.form-group.row.ng-star-inserted > div > p"));
        public static IWebElement BankNameError=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > form > div > div > div:nth-child(2) > div > div"));

        public static void MakePayment(string routingNumber,string accountNumber)
        {
            RoutingNumber.SendKeys(routingNumber);
            AccountNumber.SendKeys(accountNumber);
            WaitForBankName();
            SubmitBtn.Click();
        }
        public static void WaitForBankName()
        {
            DriverExtensions.WaitForElementToLoad(BankName, 5);
        }
    }
}
