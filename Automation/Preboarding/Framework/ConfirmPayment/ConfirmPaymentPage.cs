﻿using OpenQA.Selenium;
using Framework;
using PreboardingWebsite.Framework.MakePayment;

namespace PreboardingWebsite.Framework.ConfirmPayment
{
    public class ConfirmPaymentPage
    {
        public static void GoTo(string routingNumber,string accountNumber)
        {
            MakePaymentPage.GoTo();
            MakePaymentPage.MakePayment(routingNumber, accountNumber);
        }

        public static IWebElement CancelBtn=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div:nth-child(3) > button.btn.sms-btn-cancel"));
        public static IWebElement SubmitBtn=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div:nth-child(3) > button:nth-child(2)"));
        public static IWebElement PaymentAmount => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div.row > div > dl > dd:nth-child(4)"));
        public static IWebElement DraftDate => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div.row > div > dl > dd:nth-child(6)"));
        public static IWebElement BankAccountType => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div.row > div > dl > dd:nth-child(8)"));
        public static IWebElement Bank => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div.row > div > dl > dd:nth-child(10)"));
        public static IWebElement RoutingNumber => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div.row > div > dl > dd:nth-child(12)"));
        public static IWebElement AccountNumber => Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div.row > div > dl > dd:nth-child(14)"));

    }
}
