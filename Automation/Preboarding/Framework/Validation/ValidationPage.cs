﻿using OpenQA.Selenium;
using Framework;
using PreboardingWebsite.Framework.Registration;

namespace PreboardingWebsite.Framework.Validation
{    
    public class ValidationPage
    {
        public static void GoTo(string loanNumber = "0578319064", string zipCode= "28660", string ssn= "4801")
        {
            RegistrationPage.GoTo();
            RegistrationPage.FillInRegistrationInformation(loanNumber, zipCode, ssn);
            RegistrationPage.SubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad(Paragraph1);
            System.Threading.Thread.Sleep(200);
        }

        public static IWebElement BorrowerName=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > p:nth-child(1)"));
        public static IWebElement WebsiteLink=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > p:nth-child(4) > a"));
        public static IWebElement SchedulePaymentBtn=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div:nth-child(6) > ol > li:nth-child(1) > div > button"));
        public static IWebElement Paragraph1=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > p:nth-child(2)"));
        public static IWebElement Paragraph2=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > p:nth-child(3)"));
        public static IWebElement Paragraph3=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > p:nth-child(4)"));

        public static string ExpectedP1(string servicerName)
        {
            return "Welcome to " + servicerName + "! We're happy to have you as a customer and look forward to serving your mortgage needs now and in the years to come. Your mortgage will transfer to " + servicerName + ".";
        }
        public static string ExpectedP2(string priorServicerName, string servicerName)
        {
            return priorServicerName + " and " + servicerName + " both recently mailed you an important letter regarding the transfer of your mortgage to " + servicerName + ".";
        }
        public static string ExpectedP3(string transferDate, string priorServerName, string servicerName, string activationDate, string webAddress)
        {
            return "The date of your transfer will be " + transferDate + ". Due to the time it takes to properly close your " + priorServerName + " account, reconcile the account and open your account at " + servicerName + 
                ", we will not be able to access your full account information until " + activationDate + ". At that time, you will be able to create your online account on our website at "+webAddress+".";
        }
    }
}
