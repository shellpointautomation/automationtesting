﻿using OpenQA.Selenium;
using Framework;

namespace PreboardingWebsite.Framework.Validation
{
    /// <summary>
    /// where AutodraftTransfer is null and HadAutodraft is null
    /// </summary>
    public class NoAutoDraftPage 
    { 
        public static IWebElement Paragraph1=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div:nth-child(5) > p"));
        public static IWebElement Paragraph2=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div:nth-child(6) > p"));
        public static string ExpectedP1(string dateRecieved)
        {
            return "Please note that your account does not have automatic payments setup as of " + dateRecieved + ".";
        }
        public static string ExpectedP2()
        {
            return "Since automatic payments are not set up, you may make your first payment using the following methods:";
        }
    }
}
