﻿using OpenQA.Selenium;
using Framework;
using PreboardingWebsite.Framework.Registration;

namespace PreboardingWebsite.Framework.Validation
{
    public class PaymentMadePage 
    {
        public static void GoTo(string loanNumber, string zipCode, string ssn)
        {
            RegistrationPage.GoTo();
            RegistrationPage.FillInRegistrationInformation(loanNumber, zipCode, ssn);
            RegistrationPage.SubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad(Paragraph1);
        }

        public static IWebElement Paragraph1=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > p"));
        public static string Paragraph1Expected = "Your payment has already been scheduled for June 8, 2018. Please visit https://myloan.newpennservicing.com after June 8, 2018 to make additional payments, or to enroll in automatic payments. Please note that we only accept the scheduling of one full-contract payment before your loan is activated with New Penn Servicing.";
        
    }
}
