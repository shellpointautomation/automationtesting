﻿using OpenQA.Selenium;
using Framework;

namespace PreboardingWebsite.Framework.Validation
{
    /// <summary>
    /// where AutodraftTransfer=1
    /// </summary>
    public class AutoDraftTransferPage 
    {
        public static IWebElement Paragraph1=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div > p:nth-child(1)"));
        public static IWebElement Paragraph2=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div > p:nth-child(2)"));
        public static IWebElement Paragraph3=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div > div > p:nth-child(3)"));
        public static string ExpectedP1(string dateReceived, string servicerName)
        {
            return "Please note that your account does have automatic payments setup as of " + dateReceived + ". " + servicerName + " will continue this service.";
        }
        public static string ExpectedP2(string draftDay)
        {
            return "Your normal payment due date is the " + draftDay + " of the month.";
        }
        public static string ExpectedP3(string servicerName)
        {
            return "Since your automatic payments will be transferred over to " + servicerName + ", no additional action is needed on your part. Please note that it may take up to eight additional days for " + servicerName + " to debit your first payment from your bank account.";
        }
    }
}
