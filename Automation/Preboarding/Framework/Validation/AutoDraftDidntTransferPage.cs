﻿using OpenQA.Selenium;
using Framework;

namespace PreboardingWebsite.Framework.Validation
{
    /// <summary>
    ///where AutodraftTransfer is null and HadAutodraft = 1
    /// </summary>
    public class AutoDraftDidntTransferPage 
    {
        public static IWebElement Paragraph1=>Driver.Instance.FindElement(By.CssSelector(""));
        public static IWebElement Paragraph2=>Driver.Instance.FindElement(By.CssSelector(""));
        public static string ExpectedP1(string servicer)
        {
            return "Please note that your ACH account information did not transfer over to " + servicer + " and will not continue this service.";               
        }
        public static string ExpectedP2 = "Since automatic payments are not set up, you may make your first payment using the following methods:";
    }
}
