﻿using NUnit.Framework;
using OpenQA.Selenium;
using Framework;

namespace PreboardingWebsite.Framework.Registration
{
    public class RegistrationPage
    {
        public static void GoTo()
        {
            string environment = TestContext.Parameters["environment"] != null ? TestContext.Parameters["environment"].ToString() : "Test";
            BaseUrl(environment);
        }

        public static IWebElement LoanNumberFld=>Driver.Instance.FindElement(By.Id("loanId"));
        public static IWebElement PropertZipFld=>Driver.Instance.FindElement(By.Id("zipCode"));
        public static IWebElement Last4TaxId=>Driver.Instance.FindElement(By.Id("redactedTaxId"));
        public static IWebElement SubmitBtn=>Driver.Instance.FindElement(By.Id("submit"));
        public static IWebElement ErrorMessage=>Driver.Instance.FindElement(By.CssSelector("body > sms-app-root > ng-component > div > div > div > div.text-danger.ng-star-inserted"));

        public static void FillInRegistrationInformation(string loanNumber, string zip, string last4SSN)
        {
            LoanNumberFld.SendKeys(loanNumber);
            PropertZipFld.SendKeys(zip);
            Last4TaxId.SendKeys(last4SSN);
        }
        public static void BaseUrl(string environment)
        {
            switch (environment)
            {
                case "Dev":
                    Driver.Instance.Navigate().GoToUrl("https://mynewloan.dev.newpennservicing.com/");
                    break;
                case "Test":
                    Driver.Instance.Navigate().GoToUrl("https://mynewloan.test.newpennservicing.com/");
                    break;
                case "PROD":
                    Driver.Instance.Navigate().GoToUrl("https://mynewloan.newpennservicing.com/");
                    break;
            }
        }
    }
}
