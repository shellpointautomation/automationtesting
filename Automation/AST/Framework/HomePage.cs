﻿using OpenQA.Selenium;
using Framework;
using System;

namespace AST.Framework
{
    public class HomePage
    {
        public static void GoTo()
        {
            string baseUrl = WebDriverSettingsReader.GetBaseUrl();
            Driver.Instance.Navigate().GoToUrl(baseUrl);
            WaitForPageToLoad.Wait();
        }
        
        public static IWebElement CalendarBtn => Driver.Instance.FindElement(By.ClassName("fa-calendar"));
        public static IWebElement RefreshBtn => Driver.Instance.FindElement(By.ClassName("fa-undo"));
        public static IWebElement WeekDateRange => Driver.Instance.FindElement(By.CssSelector("body > sms-root > ng-component > div > div > div.order-2.order-xl-1.col-xl-6.offset-xl-3.bg-white.p-5 > div.container.font-weight-bold.pb-2 > div:nth-child(2) > div.order-1.col-12.order-sm-2.col-sm-8.col-md-4.col-xl-6.d-flex.justify-content-center > div > div:nth-child(1)"));
    }
}
