﻿using OpenQA.Selenium;
using Framework;

namespace AST.Framework
{
    public class CalendarWindow
    {
        public static IWebElement MonthDropdown => Driver.Instance.FindElement(By.CssSelector("body > sms-root > ng-component > div > div > div.order-2.order-xl-1.col-xl-6.offset-xl-3.bg-white.p-5 > div.container.font-weight-bold.pb-2 > div:nth-child(2) > div.order-1.col-12.order-sm-2.col-sm-8.col-md-4.col-xl-6.d-flex.justify-content-center > div > div.d-flex.justify-content-center > form > ngb-datepicker > div.ngb-dp-header.bg-light > ngb-datepicker-navigation > ngb-datepicker-navigation-select > select:nth-child(1)"));
        public static IWebElement YearDropdown => Driver.Instance.FindElement(By.CssSelector("body > sms-root > ng-component > div > div > div.order-2.order-xl-1.col-xl-6.offset-xl-3.bg-white.p-5 > div.container.font-weight-bold.pb-2 > div:nth-child(2) > div.order-1.col-12.order-sm-2.col-sm-8.col-md-4.col-xl-6.d-flex.justify-content-center > div > div.d-flex.justify-content-center > form > ngb-datepicker > div.ngb-dp-header.bg-light > ngb-datepicker-navigation > ngb-datepicker-navigation-select > select:nth-child(2)"));
        public static IWebElement NextMonth => Driver.Instance.FindElement(By.CssSelector("[title = 'Next month']"));
        public static IWebElement PreviousMonth => Driver.Instance.FindElement(By.CssSelector("[title = 'Previous month']"));
        public static IWebElement SelectedDates => Driver.Instance.FindElement(By.CssSelector("body > sms-root > ng-component > div > div > div.order-2.order-xl-1.col-xl-6.offset-xl-3.bg-white.p-5 > div.container.font-weight-bold.pb-2 > div:nth-child(2) > div.order-1.col-12.order-sm-2.col-sm-8.col-md-4.col-xl-6.d-flex.justify-content-center > div > div.d-flex.justify-content-center > form > ngb-datepicker > div.ngb-dp-header.bg-light > ngb-datepicker-navigation > ngb-datepicker-navigation-select"));
        public static IWebElement Month(string month)
        {
            return Driver.Instance.FindElement(By.CssSelector("[aria-label = '" + month + "']"));
        }
        public static IWebElement Year(string year)
        {
            return Driver.Instance.FindElement(By.CssSelector("[value = '" + year + "']"));
        }
        public static IWebElement Day(string day)
        {
            return Driver.Instance.FindElement(By.CssSelector("[aria-label = '" + day + "']"));
        }
    }
}
