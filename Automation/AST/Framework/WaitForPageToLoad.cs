﻿using Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AST.Framework
{
    public class WaitForPageToLoad
    {
        public static IWebElement OverLay => Driver.Instance.FindElement(By.ClassName("ng-animating"));
        public static IWebElement Fade => Driver.Instance.FindElement(By.ClassName("ng-star-inserted"));
        public static void Wait()
        {
            try
            {
                Thread.Sleep(500);             
                DriverExtensions.WaitForElementDissapears(OverLay);
                DriverExtensions.WaitForElementDissapears(Fade);
                Thread.Sleep(100);
            }
            catch (Exception e)
            {
                Console.WriteLine("Wait for page exception:"+e);
            }  
        }
    }
}
