﻿using OpenQA.Selenium;
using Framework;

namespace AST.Framework
{
    public class NavBar
    {
        public static IWebElement HomeBtn => Driver.Instance.FindElement(By.ClassName("navbar-brand"));
        public static IWebElement ManageShiftsBtn => Driver.Instance.FindElement(By.CssSelector("a[href = '/shifts']"));
        public static IWebElement MyAppointmentsBtn => Driver.Instance.FindElement(By.CssSelector("a[href = '/'][class = 'nav-link']"));
        public static IWebElement SearchBar => Driver.Instance.FindElement(By.Id("spocSearch"));
    }
}
