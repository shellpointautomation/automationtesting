﻿using AST.Framework;
using Framework;
using NUnit.Framework;
using Tests;

namespace AST.Tests
{
    [TestFixture]
    public class NavbarTests : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            HomePage.GoTo();
        }
        
        [Test]
        public void MyAppointmentBtnNavigatesToCorrectPage()
        {
            NavBar.MyAppointmentsBtn.Click();
            Assert.AreEqual(WebDriverSettingsReader.GetBaseUrl(), Driver.Instance.Url);
        }
        [Test]
        public void ManageShiftsBtnNavigatesToCorrectPage()
        {
            NavBar.ManageShiftsBtn.Click();
            Assert.NotNull(ManageShiftsPage.SubmitBtn.Displayed);
        }
        [Test]
        public void HomeBtnNavigatesToCorrectPage()
        {
            NavBar.HomeBtn.Click();
            Assert.AreEqual(WebDriverSettingsReader.GetBaseUrl(), Driver.Instance.Url);
        }

    }
}
