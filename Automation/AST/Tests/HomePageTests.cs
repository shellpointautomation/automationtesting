﻿using AST.Framework;
using NUnit.Framework;
using Tests;

namespace AST.Tests
{
    [TestFixture]
    public class HomePageTests : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            HomePage.GoTo();
        }
        [Test]
        public void CalendarNextMonthNavigation()
        {
            HomePage.CalendarBtn.Click();
            CalendarWindow.Year("2020").Click();
            CalendarWindow.Month("January").Click();
            CalendarWindow.NextMonth.Click();           
            Assert.AreEqual("2020-2-1", CalendarWindow.SelectedDates.GetAttribute("ng-reflect-date"));
        }
        [Test]
        public void CalendarPreviousMonthNavigation()
        {
            HomePage.CalendarBtn.Click();
            CalendarWindow.Year("2020").Click();
            CalendarWindow.Month("January").Click();
            CalendarWindow.PreviousMonth.Click();
            Assert.AreEqual("2019-12-1", CalendarWindow.SelectedDates.GetAttribute("ng-reflect-date"));
        }
        [Test]
        public void WeekChangesWhenSelectingFromCalendarWindow()
        {
            HomePage.CalendarBtn.Click();
            CalendarWindow.Year("2020").Click();
            CalendarWindow.Month("March").Click();
            CalendarWindow.Day("Saturday, March 28, 2020").Click();
            Assert.AreEqual("3/23/2020 - 3/27/2020", HomePage.WeekDateRange.Text);
        }
    }
}
