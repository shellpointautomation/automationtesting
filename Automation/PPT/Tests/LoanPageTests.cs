﻿using NUnit.Framework;
using PPT.Framework;
using Tests;

namespace PPT.Tests

{
    [TestFixture]
    public class LoanPageTests : BaseTest
    {
        /// <summary>
        /// Opens the PPT page and navigates to the loan page for each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            LoanPage.GoTo();         
        }
        /// <summary>
        /// Checks to make sure the Alerts header is correct
        /// </summary>
        [Test]
        public void CheckAlertsHeader()
        {
            Assert.AreEqual("Alerts", LoanPage.AlertHeader.Text);
        }
        /// <summary>
        /// Checks to make sure the Tasks header is correct
        /// </summary>
        [Test]
        public void CheckTasksHeader()
        {
            Assert.AreEqual("Tasks", LoanPage.TaskHeader.Text);
        }
        /// <summary>
        /// Checks to make sure the Bids header is correct
        /// </summary>
        [Test]
        public void CheckBidsHeader()
        {
            Assert.AreEqual("Open Bids", LoanPage.OpenBidHeader.Text);
        }
        /// <summary>
        /// Checks to make sure the Damages header is correct
        /// </summary>
        [Test]
        public void CheckDamagesHeader()
        {
            Assert.AreEqual("Reported Damages", LoanPage.ReportDamagesHeader.Text);
        }
        /// <summary>
        /// Checks to make sure the DA header is correct
        /// </summary>
        [Test]
        public void CheckDAHeader()
        {
            Assert.AreEqual("Delegated Authority Requests", LoanPage.DARequestHeader.Text);
        }
        /// <summary>
        ///  Test to make sure the next button will take you to a new loan
        /// </summary>
        [Test]
        public void NextBtn()
        {
            string InitialLoanId = LoanPage.LoanNumber.Text;
            LoanPage.NextBtn.Click();
            System.Threading.Thread.Sleep(2000);
            string FinalLoanId = LoanPage.LoanNumber.Text;
            Assert.AreNotEqual(InitialLoanId, FinalLoanId);
        }
        /// <summary>
        /// You are able to search for a loan that was already in the queue previously 
        /// </summary>
        [Test]
        public void SearchForPreviousLoanInQueue()
        {
            string firstLoanTxt = LoanPage.LoanNumber.Text;
            string firstLoanId = firstLoanTxt.Substring(5,9);//shorten all text to just the loan number
            LoanPage.NextBtn.Click();
            System.Threading.Thread.Sleep(2000);          
            LoanPage.SearchBar.SendKeys(firstLoanId);
            LoanPage.SearchBtn.Click();
            System.Threading.Thread.Sleep(2000);
            string finalLoanTxt = LoanPage.LoanNumber.Text;
            string finalLoanId = finalLoanTxt.Substring(5, 9);//shorten all text to just the loan number
            Assert.AreEqual(firstLoanId, finalLoanId);

        }
    }
}
