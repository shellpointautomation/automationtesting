﻿using NUnit.Framework;
using Framework;
using Tests;
using PPT.Framework;

namespace PPT.Tests
{
    [TestFixture]
    public class HomePageTests : BaseTest
    {
        /// <summary>
        /// Setup that occurs before each test is run
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            HomePage.GoTo();
        }
        /// <summary>
        /// Test each of the options in the Processor dropdown and validates they can be selected
        /// </summary>

        /// <param name="dropdownPosition"></param>
        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(6)]
        public void SelectProcessorFromDropdown(int dropdownPosition)
        {   
            HomePage.ProcessorDropdown.Click();
            HomePage.ProcessorFromDropdown(dropdownPosition).Click();
            Assert.IsNotNull(HomePage.GetProcessorValue());
        }
        /// <summary>
        /// Test each of the options in the Vendor dropdown and validates they can be selected
        /// </summary>
        /// <param name="dropdownPosition"></param>
        [Test]
        [TestCase(0)]
        [TestCase(1)]
        public void SelectVendorFromDropdown(int dropdownPosition)
        {
            HomePage.VendorDropdown.Click();
            HomePage.VendorFromDropdown(dropdownPosition).Click();
            Assert.IsNotNull(HomePage.GetVendorValue());
        }
        /// <summary>
        /// Test each of the options in the Client dropdown and validates they can be selected
        /// </summary>
        /// <param name="dropdownPosition"></param>
        [Test]
        [TestCase(0)]         [TestCase(1)]         [TestCase(2)]         [TestCase(3)]
        [TestCase(4)]         [TestCase(5)]         [TestCase(6)]         [TestCase(7)]
        [TestCase(8)]         [TestCase(9)]         [TestCase(10)]        [TestCase(11)]
        [TestCase(12)]        [TestCase(13)]        [TestCase(14)]        [TestCase(15)]
        [TestCase(16)]        [TestCase(17)]        [TestCase(18)]        [TestCase(19)]
        [TestCase(20)]        [TestCase(21)]        [TestCase(22)]        [TestCase(23)]
        [TestCase(24)]        [TestCase(25)]        [TestCase(26)]        [TestCase(27)]
        [TestCase(28)]        [TestCase(29)]        [TestCase(30)]        [TestCase(31)]      
        public void SelectClientFromDropdown(int dropdownPosition)
        {
            HomePage.ClientDropdown.Click();
            HomePage.ClientFromDropdown(dropdownPosition).Click();
            Assert.IsNotNull( HomePage.GetClientValue());
        }
        /// <summary>
        /// Tests that the search button works for the loan queue
        /// </summary>
        [Test]
        public void LoanQueueSearchButtonNoCriteria()
        {
            HomePage.LoanQueueSubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad(LoanPage.LoanNumber);
            Assert.AreEqual(true, LoanPage.LoanNumber.Displayed);
        }
        /// <summary>
        /// Tests that the search button works for the loan queue when one dropdown is used
        /// </summary>
        [Test]
        public void LoanQueueSearchButtonOneCriteria()
        {
            HomePage.ProcessorDropdown.Click();
            HomePage.ProcessorFromDropdown(0).Click();
            HomePage.LoanQueueSubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad(LoanPage.LoanNumber);
            Assert.AreEqual(true, LoanPage.LoanNumber.Displayed);
        }
        /// <summary>
        /// Tests that the search button works for the loan queue when two dropdowns are used
        /// </summary>
        [Test]
        public void LoanQueueSearchButtonTwoCriteria()
        {
            HomePage.ProcessorDropdown.Click();
            HomePage.ProcessorFromDropdown(0).Click();
            HomePage.VendorDropdown.Click();
            HomePage.VendorFromDropdown(0).Click();
            HomePage.LoanQueueSubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad( LoanPage.LoanNumber);
            Assert.AreEqual(true, LoanPage.LoanNumber.Displayed);
        }
        /// <summary>
        /// Tests that the search button works for the loan queue when all dropdowns are selected
        /// </summary>
        [Test]
        public void LoanQueueSearchButtonAllCriteria()
        {
            HomePage.ProcessorDropdown.Click();
            HomePage.ProcessorFromDropdown( 0).Click();
            HomePage.VendorDropdown.Click();
            HomePage.VendorFromDropdown(0).Click();
            HomePage.ClientDropdown.Click();
            HomePage.ClientFromDropdown(4).Click();
            HomePage.LoanQueueSubmitBtn.Click();
            DriverExtensions.WaitForElementToLoad(LoanPage.LoanNumber);
            Assert.AreEqual(true, LoanPage.LoanNumber.Displayed);
        }
        /// <summary>
        /// Tests that the reset button works on the Processor dropdown
        /// </summary>
        [Test]
        public void ResetButtonProcessor()
        {
            HomePage.ProcessorDropdown.Click();
            HomePage.ProcessorFromDropdown(0).Click();
            HomePage.ResetBtn.Click();
            Assert.AreEqual("-- ALL --", HomePage.GetProcessorValue());
        }
        /// <summary>
        /// Tests that the reset button works on the Vendor dropdown
        /// </summary>
        [Test]
        public void ResetButtonVendor()
        {
            HomePage.VendorDropdown.Click();
            HomePage.VendorFromDropdown(0).Click();
            HomePage.ResetBtn.Click();
            Assert.AreEqual("-- ALL --", HomePage.GetVendorValue());
        }
        /// <summary>
        /// Tests that the reset button works on the Client dropdown
        /// </summary>
        [Test]
        public void ResetButtonClient()
        {
            HomePage.ClientDropdown.Click();
            HomePage.ClientFromDropdown(0).Click();     
            HomePage.ResetBtn.Click();
            Assert.AreEqual("-- ALL --",HomePage.GetClientValue());
        }
    }
}
