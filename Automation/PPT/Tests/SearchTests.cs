﻿using NUnit.Framework;
using Framework;
using Tests;
using PPT.Framework;

namespace PPT.Tests
{
    [TestFixture]
    public class SearchTests : BaseTest
    {
        /// <summary>
        /// Setup that occurs before each test is run
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            HomePage.GoTo();
        }
        /// <summary>
        /// Test to make sure the correct error message is displayed for '12345678'
        /// </summary>
        [Test]
        public void SearchTooFewCharacters()
        {           
            SearchPage.SearchFld.SendKeys("12345678");
            SearchPage.SearchBtn.Click();
            Assert.AreEqual(SearchPage.MustBeSearchError, SearchPage.SearchErrorMessage.Text);
        }
        /// <summary>
        /// Test to make sure the correct error message is displayed for '123456789012'
        /// </summary>
        [Test]
        public void SearchTooManyCharacters()
        {
            SearchPage.SearchFld.SendKeys("123456789012");
            SearchPage.SearchBtn.Click();
            Assert.AreEqual(SearchPage.MustBeSearchError, SearchPage.SearchErrorMessage.Text);
        }
        /// <summary>
        /// Test to make sure the correct error message is displayed for 'abcdefghi'
        /// </summary>
        [Test]
        public void SearchLetters()
        {
            SearchPage.SearchFld.SendKeys("abcdefghi");
            SearchPage.SearchBtn.Click();
            Assert.AreEqual(SearchPage.MustBeSearchError, SearchPage.SearchErrorMessage.Text);
        }
        /// <summary>
        /// Test to make sure the correct error message is displayed when searching a blank loan ID
        /// </summary>

        [Test]
        public void SearchBlank()
        {
            SearchPage.SearchBtn.Click();
            Assert.AreEqual(SearchPage.LoanRequiredSearchError, SearchPage.SearchErrorMessage.Text);
        }
        /// <summary>
        /// Search for a valid loan ID, Make sure you are taken to the loan page
        /// </summary>
        /// <param name="loanId"></param>
        [Test]
        [TestCase("123456789")]
        [TestCase("0578159146")]
        [TestCase("0578159497")]
        public void SearchValid(string loanId)
        {
            SearchPage.SearchFld.SendKeys(loanId);
            SearchPage.SearchBtn.Click();
            DriverExtensions.WaitForElementToLoad(LoanPage.AlertHeader);
            Assert.AreEqual("Loan " + loanId , LoanPage.LoanNumber.Text);
        }
    }
}
