﻿using OpenQA.Selenium;
using Framework;

namespace PPT.Framework
{
    public class SearchPage
    {
        public static IWebElement SearchFld=>Driver.Instance.FindElement(By.CssSelector("#bs-example-navbar-collapse-1 > sms-loan-search > form > div > input"));
        public static IWebElement SearchBtn=>Driver.Instance.FindElement(By.CssSelector("#bs-example-navbar-collapse-1 > sms-loan-search > form > button"));
        public static IWebElement SearchErrorMessage=>Driver.Instance.FindElement(By.CssSelector("#bs-example-navbar-collapse-1 > sms-loan-search > form > div > em"));
        // Expected error message when loanId is wrong length or contains characters
        public static string MustBeSearchError = "Loan must be 9 or 10 digits";
        // Expected error message when no loanId is entered
        public static string LoanRequiredSearchError = "Loan is required";
    }
}
