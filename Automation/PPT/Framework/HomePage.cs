﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using Framework;


namespace PPT.Framework
{
    public class HomePage
    {
        public static void GoTo()
        {
            string url = WebDriverSettingsReader.GetBaseUrl();
            BaseUrl(url);
        }
        public static Dictionary<string, int> dropDownDict = new Dictionary<string, int>()
        {
            {"Processor",1},
            {"Vendor",2 },
            {"Client",3 }
        };
        private static string DropDownValues(string dropdownName, int PositionInDropdown)
        {
            int dropdownNumber = dropDownDict[dropdownName];
            return "body > sms-root > div > sms-loan-queue > div > div > form > div:nth-child(" + dropdownNumber + ") > select > option:nth-child(" + (PositionInDropdown + 2) + ")";
        }
        public static IWebElement ResetBtn=> Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-queue > div > div > form > div.row > div.col-xs-6.reset-button > button"));
        public static IWebElement LoanQueueSubmitBtn=> Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-queue > div > div > form > div.row > div.col-xs-6.submit-button > button"));        
        public static IWebElement ProcessorDropdown =>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-queue > div > div > form > div:nth-child(1) > select"));       
        public static IWebElement VendorDropdown=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-queue > div > div > form > div:nth-child(2) > select"));
        public static IWebElement ClientDropdown=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-queue > div > div > form > div:nth-child(3) > select"));        
        public static IWebElement ProcessorFromDropdown(int dropdownPosition)=> Driver.Instance.FindElement(By.CssSelector(DropDownValues("Processor", dropdownPosition)));
        public static IWebElement VendorFromDropdown(int dropdownPosition)=>Driver.Instance.FindElement(By.CssSelector(DropDownValues("Vendor", dropdownPosition)));        
        public static IWebElement ClientFromDropdown(int dropdowPosition)=>Driver.Instance.FindElement(By.CssSelector(DropDownValues("Client", dropdowPosition)));
        public static string GetProcessorValue()
        {
            SelectElement selectedElement = new SelectElement(ProcessorDropdown);
            return selectedElement.SelectedOption.Text;
        }
        public static string GetVendorValue()
        {
            SelectElement selectedElement = new SelectElement(VendorDropdown);
            return selectedElement.SelectedOption.Text;
        }
        public static string GetClientValue()
        {
            SelectElement selectedElement = new SelectElement(ClientDropdown);
            return selectedElement.SelectedOption.Text;
        }
        public static void BaseUrl(string environment)
        {
            switch (environment)
            {
                case "Dev":
                    Driver.Instance.Navigate().GoToUrl("http://formscolmgmtdev/");
                    break;
                case "Test":
                    Driver.Instance.Navigate().GoToUrl("http://formscolmgmttest/");
                    break;
                case "Prod":
                    Driver.Instance.Navigate().GoToUrl("http://formscolmgmt/");
                    break;
            }
        }
    }
}
