﻿using OpenQA.Selenium;
using Framework;

namespace PPT.Framework
{
    public class LoanPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("http://formscolmgmttest/");
            HomePage.LoanQueueSubmitBtn.Click();
        }
        public static IWebElement HomeBtn=> Driver.Instance.FindElement(By.TagName("a"));
        public static IWebElement SearchBar=> Driver.Instance.FindElement(By.CssSelector("#bs-example-navbar-collapse-1 > sms-loan-search > form > div > input"));
        public static IWebElement SearchBtn=>Driver.Instance.FindElement(By.CssSelector("#bs-example-navbar-collapse-1 > sms-loan-search > form > button"));
        public static IWebElement NextBtn=> Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h2 > button"));
        public static IWebElement LoanNumber=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h2"));
        public static IWebElement AlertHeader=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h3:nth-child(2)"));
        public static IWebElement TaskHeader=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h3:nth-child(4)"));
        public static IWebElement OpenBidHeader=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h3:nth-child(6)"));
        public static IWebElement ReportDamagesHeader=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h3:nth-child(8)"));
        public static IWebElement DARequestHeader=>Driver.Instance.FindElement(By.CssSelector("body > sms-root > div > sms-loan-detail > h3:nth-child(10)"));       
    }
}
