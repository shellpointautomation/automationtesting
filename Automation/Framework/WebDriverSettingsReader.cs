﻿using System.IO;
using System.Xml.Linq;

namespace Framework
{
    public class WebDriverSettingsReader
    {
        public static string GetBrowserType()
        {
            var file = GetSettingsFilePath();
            var xel = XElement.Load(file);
            return xel.Element("browser").Element("type").Value;
        }
        public static string GetBaseUrl()
        {
            var file = GetSettingsFilePath();
            var xel = XElement.Load(file);
            var url = xel.Element("url").Element("base").Value;
            return url;
        }
        private static string GetSettingsFilePath()
        {
            // the settings file is at the same level as the executing assembly
            var settingsFileDir = Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var settingsFilePath = Path.Combine(settingsFileDir, "webDriverSettings.xml");
            return settingsFilePath;
        }
    }
}
