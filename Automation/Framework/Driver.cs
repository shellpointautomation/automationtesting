﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace Framework
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }
        public static void Initialize()
        {
            var browser = WebDriverSettingsReader.GetBrowserType();
            switch (browser)
            {
                case "Chrome":
                    Instance = new ChromeDriver();
                    break;
                case "Firefox":
                    Instance = new FirefoxDriver();
                    break;
                case "IE":
                    Instance = new InternetExplorerDriver();
                    break;
            }
            TurnOnWait();
            MaximizeWindow();
        }
        private static void TurnOnWait()
        {
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        private static void MaximizeWindow()
        {
            Instance.Manage().Window.Maximize();
        }
    }
}
