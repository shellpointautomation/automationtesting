﻿using System;

namespace Framework
{
    public class Logger
    {
        public static void Log(string msg)
        {
            Console.WriteLine(DateTime.Now.ToString("[yyyy-MM-dd HH':'mm':'ss] - ") + msg);
        }
    }
}
