﻿using OpenQA.Selenium;
using System;
using System.Drawing.Imaging;
using System.IO;

namespace Framework
{
    public class ScreenShotTaker
    {
        public static void TakeScreenshot(IWebDriver driver, string testName)
        {
            var screenShot = ((ITakesScreenshot)driver).GetScreenshot();
            new FileInfo(FilePath).Directory.Create();
            var fileName = Time + testName +"_"+ Browser + ".png";
            screenShot.SaveAsFile(FilePath+fileName);
        }
        public static string Browser = WebDriverSettingsReader.GetBrowserType();
        public static string FilePath => @"C:\Users\woconnell\Desktop\ScreenShots\" + Date;
        public static string Date => DateTime.Now.Year + "\\" + DateTime.Now.Month + "\\" + DateTime.Now.Day + "\\";
        public static string Time => DateTime.Now.Hour + Minutes() + "_";        
        public static string Minutes()
        {
            string minutes = "";
            if (DateTime.Now.Minute< 10)
            {
                minutes = "0" + DateTime.Now.Minute;               
            }
            else
            {
                minutes = DateTime.Now.Minute.ToString();
            }
            return minutes;
        }
    }
}
