﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace Framework
{
    public static class DriverExtensions 
    {
        public static void ScrollToElement(IWebElement element)
        {
            Actions actions = new Actions(Driver.Instance);
            actions.MoveToElement(element);
            actions.Perform();
        }
        public static void GlobalSendEscapeKey()
        {
            Actions actions = new Actions(Driver.Instance);
            actions.SendKeys(Keys.Escape).Perform();
        }
        public static IWebElement WaitForElementToLoad(IWebElement element, int timeout = 10)
        {
            try
            {
                var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(timeout));
                return wait.Until(ExpectedConditions.ElementToBeClickable(element));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + element + "' was not found in current context page.");
                throw;
            }
        }
        public static bool WaitForElementDissapears(IWebElement element, int timeout = 10)
        {
            try
            {
                var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
                return wait.Until(driver => !element.Displayed);
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + element + "' was not found in current context page.");
                throw;
            }
        }
    }
}
