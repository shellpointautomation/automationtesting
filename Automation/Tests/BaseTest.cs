﻿using Framework;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace Tests
{
    [SetUpFixture]
    public class BaseTest
    {
        /// <summary>
        /// Launches the browser to start the test fixture
        /// </summary>
        [OneTimeSetUp]
        virtual public void LaunchBrowser()
        {           
            Driver.Initialize();
        }
        [TearDown]
        virtual public void TearDown()
        {
            string testName = TestContext.CurrentContext.Test.MethodName;
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                ScreenShotTaker.TakeScreenshot(Driver.Instance, testName);
            }
        }
        /// <summary>
        /// When the testfixture has been run the window will be closed
        /// </summary>
        [OneTimeTearDown]
        virtual public void CloseBrowser()
        {
            Driver.Instance.Close();
        }
    }
}
